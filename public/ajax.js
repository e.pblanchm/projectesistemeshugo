function logout() {
    localStorage.removeItem("isLoggedIn");

    location.reload();
}


function getUsers() {
    $.ajax({
        type: 'GET',
        url: 'Get-users.php',
        success: function(response) {
            $('#result').css('color', 'green').html(JSON.stringify(response, null, 2));
        },
        error: function(error) {
            $('#result').css('color', 'red').html('Error: ' + JSON.stringify(error, null, 2));
        }
    });
}
function register() {
    var email = $('#email').val();
    var credencial = $('#credencial').val();

    $.ajax({
        type: 'POST',
        url: 'Register.php',
        contentType: 'application/json',
        data: JSON.stringify({ email: email, credencial: credencial }),
        success: function(response) {
            response = JSON.parse(response);

            if (response.estat === "OK") {
                $('#result').css('color', 'green').html('Usuari registrat: ' + response.usuari_app);
            } else {
                $('#result').css('color', 'red').html('Error: ' + response.error);
            }
        },
        error: function(error) {
            $('#result').css('color', 'red').html('Error: ' + JSON.stringify(error, null, 2));
        }
    });
}

function login() {
    var email = $('#email').val();
    var credencial = $('#credencial').val();

    $.ajax({
        type: 'POST',
        url: 'Login.php',
        contentType: 'application/json',
        data: JSON.stringify({ email: email, credencial: credencial }),
        success: function(response) {
            response = JSON.parse(response);

            if (response.estat === "OK") {
                $('#result').css('color', 'green').html('Inici de sessió reeixit per: ' + response.usuari_app);
            } else {
                $('#result').css('color', 'red').html('Error: ' + response.error);
            }
        },
        error: function(error) {
            $('#result').css('color', 'red').html('Error: ' + JSON.stringify(error, null, 2));
        }
    });
}